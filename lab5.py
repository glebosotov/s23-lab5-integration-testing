import httpx

api_key = 'AKfycbzp093GuTsC8jWmn6v8zWvaUAw0LErgD-P8VXt71Ll8-y2lewGh0Mw6sFyg6truO66T'
email = 'g.osotov@innopolis.university'

'''
Here is InnoCar Specs:
Budet car price per minute = 22
Luxury car price per minute = 64
Fixed price per km = 20
Allowed deviations in % = 5
Inno discount in % = 10
'''

budget_car_price_per_minute = 22
luxury_car_price_per_minute = 64
fixed_price_per_km = 20
allowed_deviations_in_percent = 5
inno_discount_in_percent = 10

types = ['budget', 'luxury']
plan = ['minute', 'fixed_price']
distance = [100, 101]
planned_distance = [100, 101]
time = [100, 101]
planned_time = [100, 101]
inno_discount = ['yes', 'no']

def api_call(
        type_param, plan_param, distance_param, planned_distance_param, time_param, planned_time_param, inno_discount_param
):
    url = 'https://script.google.com/macros/s/' + api_key + '/exec'
    params = {
        'service': 'calculatePrice',
        'email': email,
        'type': type_param,
        'plan': plan_param,
        'distance': distance_param,
        'planned_distance': planned_distance_param,
        'time': time_param,
        'planned_time': planned_time_param,
        'inno_discount': inno_discount_param
    }
    response = httpx.get(url, params=params, follow_redirects=True, timeout=20)
    return response.text if response.text == 'Invalid Request' else response.json()['price']

def expected_result(
        type_param, plan_param, distance_param, planned_distance_param, time_param, planned_time_param, inno_discount_param
):
    if type_param is None or type_param not in types:
        return 'Invalid Request'
    if plan_param is None or plan_param not in plan:
        return 'Invalid Request'
    if distance_param < 0:
        return 'Invalid Request'
    if planned_distance_param < 0:
        return 'Invalid Request'
    if time_param < 0:
        return 'Invalid Request'
    if planned_time_param < 0:
        return 'Invalid Request'
    if inno_discount_param is None or inno_discount_param not in inno_discount:
        return 'Invalid Request'
    
    # calculate price
    '''
    Then let's read through the description of our system:
    The InnoDrive is a hypothetical car sharing service that provides transportations means in Kazan region including Innopolis. The service proposes two types of cars budget and luxury. The service offers flexible two tariff plans. With the minute plan, the service charges its client by time per minute of use. The Service also proposes a fixed_price plan whereas the price is fixed at the reservation time when the route is chosen. If the driver deviates from the planned route for more than n%, the tariff plan automatically switches to the minute plan. The deviation is calculated as difference in the distance or duration from the originally planned route. The fixed_price plan is available for budget cars only. The Innopolis city sponsors the mobility and offers m% discount to Innopolis residents.
    Imagine you are the quality engineer of Innodrive. How are you going to know about application quality???
    '''
    
    # take diviation into account
    if plan_param == 'minute':
        if distance_param > planned_distance_param * (1 + allowed_deviations_in_percent / 100):
            plan_param = 'minute'
        if time_param > planned_time_param * (1 + allowed_deviations_in_percent / 100):
            plan_param = 'minute'
    
    # calculate price
    if type_param == 'budget':
        if plan_param == 'minute':
            price = time_param * budget_car_price_per_minute
        else:
            price = distance_param * fixed_price_per_km
    else:
        if plan_param == 'minute':
            price = time_param * luxury_car_price_per_minute
        else:
            return 'Invalid Request'
    
    # apply discount
    if inno_discount_param == 'yes':
        price *= (1 - inno_discount_in_percent / 100)

    # return price as string with only meanu digits after dot
    if (price * 1.0).is_integer():
        return str(int(price))
    else:
        return str(round(price, 1))


def iterate_params():
    for type_param in types:
        for plan_param in plan:
            for distance_param in distance:
                for planned_distance_param in planned_distance:
                    for time_param in time:
                        for planned_time_param in planned_time:
                            for inno_discount_param in inno_discount:
                                yield type_param, plan_param, distance_param, planned_distance_param, time_param, planned_time_param, inno_discount_param

def test_invalid_types():
    test_cases = [
        ('invalid_type', 'minute', 100, 100, 100, 100, 'yes'),
        ('budget', 'invalid_plan', 100, 100, 100, 100, 'yes'),
        ('budget', 'minute', -1, 100, 100, 100, 'yes'),
        ('budget', 'minute', 100, -1, 100, 100, 'yes'),
        ('budget', 'minute', 100, 100, -1, 100, 'yes'),
        ('budget', 'minute', 100, 100, 100, -1, 'yes'),
        ('budget', 'minute', 100, 100, 100, 100, 'invalid_discount'),
    ]
    for type_param, plan_param, distance_param, planned_distance_param, time_param, planned_time_param, inno_discount_param in test_cases:
        api =  api_call(
            type_param, plan_param, distance_param, planned_distance_param, time_param, planned_time_param, inno_discount_param
        )
        api = str(api) 
        expected = expected_result(
            type_param, plan_param, distance_param, planned_distance_param, time_param, planned_time_param, inno_discount_param
        )
        passed = '✅' if api == expected else '❌'
        print(f'| {type_param} | {plan_param} | {distance_param} | {planned_distance_param} | {time_param} | {planned_time_param} | {inno_discount_param} | {api} | {expected} | {passed}')

def test_calculations():
    for type_param, plan_param, distance_param, planned_distance_param, time_param, planned_time_param, inno_discount_param in iterate_params():
        api =  api_call(
            type_param, plan_param, distance_param, planned_distance_param, time_param, planned_time_param, inno_discount_param
        )
        api = str(api) 
        expected = expected_result(
            type_param, plan_param, distance_param, planned_distance_param, time_param, planned_time_param, inno_discount_param
        )
        passed = '✅' if api == expected else '❌'
        print(f'| {type_param} | {plan_param} | {distance_param} | {planned_distance_param} | {time_param} | {planned_time_param} | {inno_discount_param} | {api} | {expected} | {passed} |')

   
if __name__ == '__main__':
    test_calculations()
    test_invalid_types()