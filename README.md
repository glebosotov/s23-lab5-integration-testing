# Lab 5

## Gleb Osotov

g.osotov@innopolis.university
BS19-SD-01

## Vars

Here is InnoCar Specs:
Budet car price per minute = 22
Luxury car price per minute = 64
Fixed price per km = 20
Allowed deviations in % = 5
Inno discount in % = 10

## BVA

| Argument         | Equivalence classes               |
|------------------|-----------------------------------|
| type             | 'budget', 'luxury', None      |
| plan             | 'minute', 'fixed_price', None |
| distance         | <=0, >0, None                 |
| planned_distance | <=0, >0, None                 |
| time             | <=0, >0, None                 |
| planned_time     | <=0, >0, None                 |
| inno_discount    | 'yes', 'no', None             |

## BVA table

| type             | plan             | distance | planned_distance | time | planned_time | inno_discount | price | expected price | pass |
|------------------|------------------|----------|------------------|------|--------------|---------------|-------|----------------|------|
| budget | minute | 100 | 100 | 100 | 100 | yes | 1980 | 1980 | ✅ |
| budget | minute | 100 | 100 | 100 | 100 | no | 2200 | 2200 | ✅ |
| budget | minute | 100 | 100 | 100 | 101 | yes | 1980 | 1980 | ✅ |
| budget | minute | 100 | 100 | 100 | 101 | no | 2200 | 2200 | ✅ |
| budget | minute | 100 | 100 | 101 | 100 | yes | 1999.8 | 1999.8 | ✅ |
| budget | minute | 100 | 100 | 101 | 100 | no | 2222 | 2222 | ✅ |
| budget | minute | 100 | 100 | 101 | 101 | yes | 1999.8 | 1999.8 | ✅ |
| budget | minute | 100 | 100 | 101 | 101 | no | 2222 | 2222 | ✅ |
| budget | minute | 100 | 101 | 100 | 100 | yes | 1980 | 1980 | ✅ |
| budget | minute | 100 | 101 | 100 | 100 | no | 2200 | 2200 | ✅ |
| budget | minute | 100 | 101 | 100 | 101 | yes | 1980 | 1980 | ✅ |
| budget | minute | 100 | 101 | 100 | 101 | no | 2200 | 2200 | ✅ |
| budget | minute | 100 | 101 | 101 | 100 | yes | 1999.8 | 1999.8 | ✅ |
| budget | minute | 100 | 101 | 101 | 100 | no | 2222 | 2222 | ✅ |
| budget | minute | 100 | 101 | 101 | 101 | yes | 1999.8 | 1999.8 | ✅ |
| budget | minute | 100 | 101 | 101 | 101 | no | 2222 | 2222 | ✅ |
| budget | minute | 101 | 100 | 100 | 100 | yes | 1980 | 1980 | ✅ |
| budget | minute | 101 | 100 | 100 | 100 | no | 2200 | 2200 | ✅ |
| budget | minute | 101 | 100 | 100 | 101 | yes | 1980 | 1980 | ✅ |
| budget | minute | 101 | 100 | 100 | 101 | no | 2200 | 2200 | ✅ |
| budget | minute | 101 | 100 | 101 | 100 | yes | 1999.8 | 1999.8 | ✅ |
| budget | minute | 101 | 100 | 101 | 100 | no | 2222 | 2222 | ✅ |
| budget | minute | 101 | 100 | 101 | 101 | yes | 1999.8 | 1999.8 | ✅ |
| budget | minute | 101 | 100 | 101 | 101 | no | 2222 | 2222 | ✅ |
| budget | minute | 101 | 101 | 100 | 100 | yes | 1980 | 1980 | ✅ |
| budget | minute | 101 | 101 | 100 | 100 | no | 2200 | 2200 | ✅ |
| budget | minute | 101 | 101 | 100 | 101 | yes | 1980 | 1980 | ✅ |
| budget | minute | 101 | 101 | 100 | 101 | no | 2200 | 2200 | ✅ |
| budget | minute | 101 | 101 | 101 | 100 | yes | 1999.8 | 1999.8 | ✅ |
| budget | minute | 101 | 101 | 101 | 100 | no | 2222 | 2222 | ✅ |
| budget | minute | 101 | 101 | 101 | 101 | yes | 1999.8 | 1999.8 | ✅ |
| budget | minute | 101 | 101 | 101 | 101 | no | 2222 | 2222 | ✅ |
| budget | fixed_price | 100 | 100 | 100 | 100 | yes | 1125 | 1800 | ❌ |
| budget | fixed_price | 100 | 100 | 100 | 100 | no | 1250 | 2000 | ❌ |
| budget | fixed_price | 100 | 100 | 100 | 101 | yes | 1125 | 1800 | ❌ |
| budget | fixed_price | 100 | 100 | 100 | 101 | no | 1250 | 2000 | ❌ |
| budget | fixed_price | 100 | 100 | 101 | 100 | yes | 1125 | 1800 | ❌ |
| budget | fixed_price | 100 | 100 | 101 | 100 | no | 1250 | 2000 | ❌ |
| budget | fixed_price | 100 | 100 | 101 | 101 | yes | 1125 | 1800 | ❌ |
| budget | fixed_price | 100 | 100 | 101 | 101 | no | 1250 | 2000 | ❌ |
| budget | fixed_price | 100 | 101 | 100 | 100 | yes | 1136.25 | 1800 | ❌ |
| budget | fixed_price | 100 | 101 | 100 | 100 | no | 1262.5 | 2000 | ❌ |
| budget | fixed_price | 100 | 101 | 100 | 101 | yes | 1136.25 | 1800 | ❌ |
| budget | fixed_price | 100 | 101 | 100 | 101 | no | 1262.5 | 2000 | ❌ |
| budget | fixed_price | 100 | 101 | 101 | 100 | yes | 1136.25 | 1800 | ❌ |
| budget | fixed_price | 100 | 101 | 101 | 100 | no | 1262.5 | 2000 | ❌ |
| budget | fixed_price | 100 | 101 | 101 | 101 | yes | 1136.25 | 1800 | ❌ |
| budget | fixed_price | 100 | 101 | 101 | 101 | no | 1262.5 | 2000 | ❌ |
| budget | fixed_price | 101 | 100 | 100 | 100 | yes | 1125 | 1818 | ❌ |
| budget | fixed_price | 101 | 100 | 100 | 100 | no | 1250 | 2020 | ❌ |
| budget | fixed_price | 101 | 100 | 100 | 101 | yes | 1125 | 1818 | ❌ |
| budget | fixed_price | 101 | 100 | 100 | 101 | no | 1250 | 2020 | ❌ |
| budget | fixed_price | 101 | 100 | 101 | 100 | yes | 1125 | 1818 | ❌ |
| budget | fixed_price | 101 | 100 | 101 | 100 | no | 1250 | 2020 | ❌ |
| budget | fixed_price | 101 | 100 | 101 | 101 | yes | 1125 | 1818 | ❌ |
| budget | fixed_price | 101 | 100 | 101 | 101 | no | 1250 | 2020 | ❌ |
| budget | fixed_price | 101 | 101 | 100 | 100 | yes | 1136.25 | 1818 | ❌ |
| budget | fixed_price | 101 | 101 | 100 | 100 | no | 1262.5 | 2020 | ❌ |
| budget | fixed_price | 101 | 101 | 100 | 101 | yes | 1136.25 | 1818 | ❌ |
| budget | fixed_price | 101 | 101 | 100 | 101 | no | 1262.5 | 2020 | ❌ |
| budget | fixed_price | 101 | 101 | 101 | 100 | yes | 1136.25 | 1818 | ❌ |
| budget | fixed_price | 101 | 101 | 101 | 100 | no | 1262.5 | 2020 | ❌ |
| budget | fixed_price | 101 | 101 | 101 | 101 | yes | 1136.25 | 1818 | ❌ |
| budget | fixed_price | 101 | 101 | 101 | 101 | no | 1262.5 | 2020 | ❌ |
| luxury | minute | 100 | 100 | 100 | 100 | yes | 5760 | 5760 | ✅ |
| luxury | minute | 100 | 100 | 100 | 100 | no | 6400 | 6400 | ✅ |
| luxury | minute | 100 | 100 | 100 | 101 | yes | 5760 | 5760 | ✅ |
| luxury | minute | 100 | 100 | 100 | 101 | no | 6400 | 6400 | ✅ |
| luxury | minute | 100 | 100 | 101 | 100 | yes | 5817.6 | 5817.6 | ✅ |
| luxury | minute | 100 | 100 | 101 | 100 | no | 6464 | 6464 | ✅ |
| luxury | minute | 100 | 100 | 101 | 101 | yes | 5817.6 | 5817.6 | ✅ |
| luxury | minute | 100 | 100 | 101 | 101 | no | 6464 | 6464 | ✅ |
| luxury | minute | 100 | 101 | 100 | 100 | yes | 5760 | 5760 | ✅ |
| luxury | minute | 100 | 101 | 100 | 100 | no | 6400 | 6400 | ✅ |
| luxury | minute | 100 | 101 | 100 | 101 | yes | 5760 | 5760 | ✅ |
| luxury | minute | 100 | 101 | 100 | 101 | no | 6400 | 6400 | ✅ |
| luxury | minute | 100 | 101 | 101 | 100 | yes | 5817.6 | 5817.6 | ✅ |
| luxury | minute | 100 | 101 | 101 | 100 | no | 6464 | 6464 | ✅ |
| luxury | minute | 100 | 101 | 101 | 101 | yes | 5817.6 | 5817.6 | ✅ |
| luxury | minute | 100 | 101 | 101 | 101 | no | 6464 | 6464 | ✅ |
| luxury | minute | 101 | 100 | 100 | 100 | yes | 5760 | 5760 | ✅ |
| luxury | minute | 101 | 100 | 100 | 100 | no | 6400 | 6400 | ✅ |
| luxury | minute | 101 | 100 | 100 | 101 | yes | 5760 | 5760 | ✅ |
| luxury | minute | 101 | 100 | 100 | 101 | no | 6400 | 6400 | ✅ |
| luxury | minute | 101 | 100 | 101 | 100 | yes | 5817.6 | 5817.6 | ✅ |
| luxury | minute | 101 | 100 | 101 | 100 | no | 6464 | 6464 | ✅ |
| luxury | minute | 101 | 100 | 101 | 101 | yes | 5817.6 | 5817.6 | ✅ |
| luxury | minute | 101 | 100 | 101 | 101 | no | 6464 | 6464 | ✅ |
| luxury | minute | 101 | 101 | 100 | 100 | yes | 5760 | 5760 | ✅ |
| luxury | minute | 101 | 101 | 100 | 100 | no | 6400 | 6400 | ✅ |
| luxury | minute | 101 | 101 | 100 | 101 | yes | 5760 | 5760 | ✅ |
| luxury | minute | 101 | 101 | 100 | 101 | no | 6400 | 6400 | ✅ |
| luxury | minute | 101 | 101 | 101 | 100 | yes | 5817.6 | 5817.6 | ✅ |
| luxury | minute | 101 | 101 | 101 | 100 | no | 6464 | 6464 | ✅ |
| luxury | minute | 101 | 101 | 101 | 101 | yes | 5817.6 | 5817.6 | ✅ |
| luxury | minute | 101 | 101 | 101 | 101 | no | 6464 | 6464 | ✅ |
| luxury | fixed_price | 100 | 100 | 100 | 100 | yes | 1125 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 100 | 100 | 100 | no | 1250 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 100 | 100 | 101 | yes | 1125 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 100 | 100 | 101 | no | 1250 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 100 | 101 | 100 | yes | 1125 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 100 | 101 | 100 | no | 1250 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 100 | 101 | 101 | yes | 1125 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 100 | 101 | 101 | no | 1250 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 101 | 100 | 100 | yes | 1136.25 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 101 | 100 | 100 | no | 1262.5 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 101 | 100 | 101 | yes | 1136.25 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 101 | 100 | 101 | no | 1262.5 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 101 | 101 | 100 | yes | 1136.25 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 101 | 101 | 100 | no | 1262.5 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 101 | 101 | 101 | yes | 1136.25 | Invalid Request | ❌ |
| luxury | fixed_price | 100 | 101 | 101 | 101 | no | 1262.5 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 100 | 100 | 100 | yes | 1125 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 100 | 100 | 100 | no | 1250 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 100 | 100 | 101 | yes | 1125 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 100 | 100 | 101 | no | 1250 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 100 | 101 | 100 | yes | 1125 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 100 | 101 | 100 | no | 1250 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 100 | 101 | 101 | yes | 1125 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 100 | 101 | 101 | no | 1250 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 101 | 100 | 100 | yes | 1136.25 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 101 | 100 | 100 | no | 1262.5 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 101 | 100 | 101 | yes | 1136.25 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 101 | 100 | 101 | no | 1262.5 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 101 | 101 | 100 | yes | 1136.25 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 101 | 101 | 100 | no | 1262.5 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 101 | 101 | 101 | yes | 1136.25 | Invalid Request | ❌ |
| luxury | fixed_price | 101 | 101 | 101 | 101 | no | 1262.5 | Invalid Request | ❌ |
| invalid_type | minute | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request | ✅ |
| budget | invalid_plan | 100 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request | ✅ |
| budget | minute | -1 | 100 | 100 | 100 | yes | Invalid Request | Invalid Request | ✅ |
| budget | minute | 100 | -1 | 100 | 100 | yes | Invalid Request | Invalid Request | ✅ |
| budget | minute | 100 | 100 | -1 | 100 | yes | Invalid Request | Invalid Request | ✅ |
| budget | minute | 100 | 100 | 100 | -1 | yes | Invalid Request | Invalid Request | ✅ |
| budget | minute | 100 | 100 | 100 | 100 | invalid_discount | Invalid Request | Invalid Request | ✅ |

## Issues

- fixed_price plan should not be allowed for luxury cars
- fixed_price is not calculated correctly for budget cars
